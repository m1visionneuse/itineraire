/*
Cette page contient les fonctions d’affichage de gestion et de synchronisation des cartes.
*/

//LES VARIABLES GLOBALES
var i=0;
var tab=[];
var markersObjectIGN={};
var markersObjectGG={};
var mark={};
var mapGG;
var map;
var mapOSM;

//LESS EVENEMENTS
document.getElementById("eff").addEventListener("click",effacement);
document.getElementById('ins').addEventListener('click',instruction);
document.getElementById("Ret").addEventListener("click",affiche);
document.getElementById("button").addEventListener("click",google);

//LES FONCTIONS
function initMap() {
  /*
  Cette fonction à pour but d’afficher et de déclencher la synchronisation des
  différentes cartes, notamment celle de google aussi elle place sur cette carte la polyline decodée.
  */

  mapGG = new google.maps.Map(document.getElementById('mapG'), {
    center: {lat: 47.3685, lng: 3.279589},
    zoom: 14,
  });
    mapGG.addListener("center_changed",synchro2);
  var flightPath = new google.maps.Polyline(
    {
    path:decoded_line,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
    }
  );
  placeMarker();
  flightPath.setMap(mapGG);
  //creation de la carte IGN
  IGN();
  //creation de la carte OSM
  OSM();
}


function OSM(){
  /*
  Cette fonction sert à initialiser et afficher un fond OSM dans une carte leaflet.
  */
  if (mapOSM==null){
    mapOSM  = L.map('mapOSM', {
      zoom : 14,
      center : L.latLng(47.3685,3.279589)
    });
    var tuileUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
    L.tileLayer(tuileUrl,{minZoom : 0, maxZoom : 18}).addTo(mapOSM);
    mapOSM.on('zoom',zoom);
    mapOSM.on('dragend',synchro);
  }
}

function IGN(){
  /*
  Cette fonction sert à initialiser et afficher un fond IGN dans une carte leaflet.
  */
  if (map==null){
    map = L.map('mapI', {
      zoom : 14,
      center : L.latLng(47.3685,3.279589)
    });
    L.tileLayer(
     'https://wxs.ign.fr/ysxcztwpwi01twxexn5lomnw/geoportail/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix={z}&tilecol={x}&tilerow={y}&layer=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD&format=image/jpeg&style=normal',
     {
       minZoom : 0,
       maxZoom : 18,
     }).addTo(map);
     map.on('zoom',zoom);
     map.on("dragend",synchro);
     map.on('click', placeMarker1);
   }
}

function zoom(e){
  /*
  Cette fonction permet lorsque le zoom d’une des cartes leaflet change d’appliquer
  le meme changement aux deux autres cartes.
  */
  mapOSM.setZoom(e.target._zoom);
  mapGG.setZoom(e.target._zoom);
  map.setZoom(e.target._zoom);
  synchro(e);
}


function synchro(e){
  /*
  Cette fonction permet lorsque le centre ou la zoom d’une des carte leaflet change
  d’appliquer le meme changement aux deux autres cartes.
  */
  mapOSM.setView(e.target.getCenter(),e.target._zoom);
  mapGG.setCenter(e.target.getCenter());
  mapGG.setZoom(e.target._zoom);
  map.setView(e.target.getCenter(),e.target._zoom)
}

function synchro2(e){
  /*
  Cette fonction permet lorsque le centre ou le zoom de la carte google change d’appliquer
  le meme changement aux deux autres cartes.
  */
  mapOSM.setView([mapGG.getCenter().lat(),mapGG.getCenter().lng()],mapGG.zoom);
  map.setView([mapGG.getCenter().lat(),mapGG.getCenter().lng()],mapGG.zoom);
}

function placeMarker(e) {
  /*
  Cette fonction permet au rechargement de la carte de replacer les marqueurs
  aux positions présentent dans tab.
  */
  mark={};
  markersObjectGG={};
  for (var i=0; i<tab.length;i++){
    markersObjectGG[i]=new google.maps.Marker({position:{lat:tab[i][1],lng:tab[i][0]},map:mapGG});
    mark[i]=L.marker(tab[i],{id:i});
    mark[i].addTo(mapOSM);
  }
}

function placeMarker1(e) {
  /*
  Cette fonction permet de placer un marqueur sur les trois cartes lorsque l’on
  clique sur celle de l’IGN. Elle détecte et gère aussi le déplacement d’un marqueur
  et la correspondance position des marqueurs /contenue des cases de tab.
  */
  lat = e.latlng.lat;
  lng = e.latlng.lng;
  markersObjectIGN[i]= L.marker([lat,lng],{
    draggable : true,
    id:i
  });
  tab.push([lat,lng]);
  if (tab.length>=2) {
    document.getElementById("button").removeAttribute("disabled");
  }
  markersObjectIGN[i].on('dragend',function(e){
    var i=e.target.options.id;
    tab[i]=[e.target._latlng.lat,e.target._latlng.lng];
    markersObjectGG[i].setPosition({lat:tab[i][0],lng:tab[i][1]});
    mark[i].setLatLng({lat:tab[i][0],lng:tab[i][1]});
    mark[i].addTo(mapOSM);
  });
  markersObjectIGN[i].addTo(map);
  markersObjectGG[i]=new google.maps.Marker({position:{lat:lat,lng:lng},map:mapGG});
  mark[i]=L.marker(tab[i],{id:i});
  mark[i].addTo(mapOSM);
  i=i+1;
}

function effacement (){
  /*
  Cette fonction permet d’effacer les marqueurs des cartes de vider le tableau
   et les résultats de requête.
  */
  tab=[];
  markersObjectGG={};
  markersObjectIGN={};
  mark=[];
  decoded_line=[];
  i=0;
  mapOSM.remove();
  map.remove();
  map=null;
  mapOSM=null;
  document.getElementById("button").setAttribute("disabled",true);
  initMap();
  // éffacement des instructions
  document.getElementById("rec_ign").innerHTML = " ";
  document.getElementById("rec_gg").innerHTML = " ";
  document.getElementById("rec").innerHTML = " ";
}

function precisionRound(number, precision) {
  /*
  Cette fonction arrondi un nombre au nombre de décimale voulu.
  Param number : il s’agit du nombre que l’on souhaite arrondir.
  Param precision : il s’agit du nombre de décimale voulu ; ce nombre doit être un entier.
  Return : le nombre arrondi.
  */
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}

function instruction(e){
  /*
  Cette fonction permet l’affichage des instructions à la place des cartes elle masque
  la div des cartes et rend visible et rempli celle des instructions.
  */
  document.getElementById("Ret").removeAttribute("disabled");
  document.getElementById("mapG").style.display="none";
  document.getElementById("mapOSM").style.display="none";
  document.getElementById("mapI").style.display="none";

  var text=document.getElementById("rec");
  text.innerHTML = ' ' ;
  text.innerHTML="Instructions:<br>"+"<h3>Temps total = "+temps_totale_osm+"min"+"<br>"+"Distance totale = "+distance_totale_osm+" km <h3><br>";
for (var j=0; j<instruction_osm.length;j++){
  for (var i =0;i<instruction_osm[j].steps.length;i++){
    text.innerHTML+="Etape "+i+" :"+"<strong>"+instruction_osm[j].steps[i]['distance']*1000+" m"+"</strong>"+","+precisionRound(instruction_osm[j].steps[i]['duration']/60,0)+"minutes "+instruction_osm[j].steps[i]['instruction']+"<br>";
  }
  text.innerHTML+="<br>"
}

  // instruction ign
  var instruct_ign=document.getElementById("rec_ign");
  instruct_ign.innerHTML = ' ' ;
  instruct_ign.innerHTML += 'Instructions:<br>' ;
  for (var k=0; k<etapes_troncon_ign.length; k++){
    etapes.push(etapes_troncon_ign[k]) ;
    instruct_ign.innerHTML += "<h3>Temps total = "+temps_totale+"<br>"+ "Distance totale = "+distance_totale+"<h3><br>"+'<h4> Etapes du point '+k+' au point '+(k+1)+ '<br>'+'distance = '+ etapes[k].distance + '<br>'+'Temps = '+ etapes[k].duration +'</h4>' ;
    for (var l=0; l<etapes[k].steps.length; l++){
      if (etapes[k].steps[l].navInstruction == 'R'){
        etapes[k].steps[l].navInstruction = 'tournez à droite'
      }else if(etapes[k].steps[l].navInstruction == 'L'){
        etapes[k].steps[l].navInstruction ='tournez à gauche'
      }else if(etapes[k].steps[l].navInstruction == 'BR'){
        etapes[k].steps[l].navInstruction = 'tournez à droite'
      }else if(etapes[k].steps[l].navInstruction == 'BL'){
        etapes[k].steps[l].navInstruction ='tournez à gauche'
      }else if(etapes[k].steps[l].navInstruction == 'B'){
        etapes[k].steps[l].navInstruction ='faites Demi-tour'
      }else if(etapes[k].steps[l].navInstruction == 'F'){
        etapes[k].steps[l].navInstruction ='allez tout droit'
      }else if(etapes[k].steps[l].navInstruction == "FR"){
        etapes[k].steps[l].navInstruction ='légèrement à droite sur'
      }else if(etapes[k].steps[l].navInstruction == "FL"){
        etapes[k].steps[l].navInstruction ='légèrement à gauche sur'
      }else if(etapes[k].steps[l].navInstruction == null){
        etapes[k].steps[l].navInstruction ='continuez tout droit sur'
      }
      steps_ign.push(etapes[k].steps[l]) ;
      instruct_ign.innerHTML += 'Etape '+[l]+ ' : '+ '<strong>' + steps_ign[l].distance+'</strong>; '+ steps_ign[l].navInstruction +' '+ steps_ign[l].name + '<br>';
     }
  }
  // instruction googlemaps
  var instruction_gg = document.getElementById("rec_gg");
  instruction_gg.innerHTML = '';
  instruction_gg.innerHTML += 'Instructions:<br>'+"<h3>Temps total = "+temps_totale_google+"<br>"+ "Distance totale = "+distance_totale_google+"<h3><br>";
  var instruction = res.routes[0].legs[0].steps;
  for (var i = 0; i < instruction.length; i++) {
      dist = instruction[i].distance.text;
      duration = instruction[i].duration.text;
      instr = instruction[i].html_instructions;
      instruction_gg.innerHTML +="Etape "+i+": "+"<strong>"+dist+"</strong>"+","+duration+" "+instr+"<br>";
  }
}

function affiche(){
  /*
  Cette fonction permet de masquer les instructions et rendre visible les cartes.
  */
  document.getElementById("rec_ign").innerHTML="";
  document.getElementById("rec_gg").innerHTML = '';
  document.getElementById("rec").innerHTML = '';
  document.getElementById("mapG").style.display="";
  document.getElementById("mapOSM").style.display="";
  document.getElementById("mapI").style.display="";
}

function replaceMarker(){

  for (var i=0;i<tab.length;i++){
   mark[i]=L.marker(tab[i],{id:i});
  mark[i].addTo(mapOSM);
    markersObjectIGN[i]=L.marker(tab[i],{id:i, draggable : true});
    markersObjectIGN[i].on('dragend',function(e){
      var i=e.target.options.id;
      tab[i]=[e.target._latlng.lat,e.target._latlng.lng];
      markersObjectGG[i].setPosition({lat:tab[i][0],lng:tab[i][1]});
    mark[i].setLatLng({lat:tab[i][0],lng:tab[i][1]});
  mark[i].addTo(mapOSM);
    });
    markersObjectIGN[i].addTo(map);
   }
}
