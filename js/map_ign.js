/*
Cette page contient les fonctions relatives à la requqte et au traitement des resultats de l'api IGN
*/

//LES VARIABLES GLOBALES
var temps_totale;
var distance_totale;
var etapes_troncon_ign;
var etapes;
var steps_ign;

//LES FONCTIONS
function swap(input, index_A, index_B) {
  /*
  Cette fonction permet d’inverser latitude et longitude dans un tableau de coordonnée.
	Param input : spécifie de tableau à traiter
	Param index_A : spécifie l’indice de la latitude de chaque point.
  Param index_B : spécifie l’indice de la longitude de chaque point.
  */
  var temp = input[index_A];
  input[index_A] = input[index_B];
  input[index_B] = temp;
}

function get_geoportail(){
  /*
  Envoie une requête ajax vers l’api de l'ign, les data envoyées comprennent la clef
  si elle existe, la liste des coordonnées saisient par l'utilisateur et les différentes
  options de calcul d'itineraire. Le callback des requêtes modifie le tracé de l'itineraire et
  la case des instructions sur la carte ign.
  */
    var features ;
    // inversion des coordonnées du tableau initial ; coordonnées saisient par l'utilisateur
    for (var i=0; i<tab.length; i++){
        swap(tab[i], 0, 1) ;
    }
    // récupération des coordonnées de départ et d'arrivée saisient par l'utilisateur
    if (tab.lenght <= 2){
      var pointDepart = tab[0];
      var pointArrive = tab[tab.length-1];
    }else{
      pointDepart = tab[0];
      pointArrive = tab[tab.length-1];
      var point_intermediaires = "";
      for (var i=1; i<tab.length-1; i++){
         point_intermediaires += tab[i]+";" ;
      }
    }
    // spécifications des options de calcul
    var methode_de_calcul = 'DISTANCE' ;
    var option_de_calcul = 'Voiture&exclusions=Toll;Tunnel' ;
    if (document.getElementById("voiture").checked){
      option_de_calcul='Voiture&exclusions=Toll;Tunnel';
    }else{
      option_de_calcul="Pieton";
    }
    if (document.getElementById("rapide").checked){
      methode_de_calcul='TIME';
    }else {
      methode_de_calcul='DISTANCE';
    }
    var key = 'ysxcztwpwi01twxexn5lomnw' ;
    /* requète ajax */
    var ajax = new XMLHttpRequest() ;
    ajax.open('GET', 'http://wxs.ign.fr/ysxcztwpwi01twxexn5lomnw/itineraire/rest/route.json?origin='+pointDepart+'&destination='+pointArrive+'&waypoints='+point_intermediaires+'&method='+methode_de_calcul+'&graphName='+option_de_calcul+'')
    ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    ajax.addEventListener('readystatechange',  function(e) {
      if(ajax.readyState == 4 && ajax.status == 200) {
        // parsing des résultats de la requète
        features = JSON.parse(ajax.responseText) ;
        // récupération du temps total du parcours
        temps_totale = features.duration ;
        distance_totale = features.distance ;
        // récupération des coordonnées dans la liste de coordonnées
        var liste_points_ign = features.geometryWkt.substr(11);
        tab_points_ign = liste_points_ign.split(',')
        var resultats_ign = [] ;
        // récupération des information sur les étapes du troncon
        etapes_troncon_ign = features.legs;
        etapes = [] ;
        steps_ign = [] ;
        for (var i=0; i<tab_points_ign.length;i++){
          // transforme en tableau chaque couple de coordonnées de chaines de caractères et les convertis en même temps en nombre
          resultats_ign.push(tab_points_ign[i].substr(1, 17).split(" ").map(Number)) ;
        }
        // réinversion des coordonnées du tableau
        for (var j=0; j<resultats_ign.length; j++){
          swap(resultats_ign[j], 0, 1) ;
        }
        // if(!trajet_ign==null){
        //   map.removeLayer(trajet_ign);
        // }
        // tracé de l'intineraire à partir de la liste des coordonnées récupérées
        var trajet_ign =new L.Polyline(resultats_ign);
        map.addLayer(trajet_ign);
        // affichage des instructions ign au clic sur le bouton instructions
        document.getElementById('ins').addEventListener('click',instruction);
        for (var i=0; i<tab.length; i++){
          swap(tab[i], 0, 1) ;
        }
      }
    });
    ajax.send();
}
