/*
Cette page contient les focntions les fonctions de requête et traitement du résultat relatif à l’api Google.
*/

//LES VARIABLES GLOBALES
var mode;
var decoded_line = [];
var res = [];
var temps_totale_google;
var distance_totale_google;

//LES FOCNTIONS
function google() {
  /*
  Cette fonction met en forme les points de cheminement et fait la requête vers l’api
  direction de google ; elle affiche aussi les résultats graphiques : pour cela elle
  relance initMap en ayant modifier la valeur de la polyline decodée.
  */
  var way_points="";
  decoded_line = [];
  var tab_inter = [];
  var instr = [];
  var dist ="";
  var duration="";
  var inst="";
  if (document.getElementById("voiture").checked){
    mode="driving";
  }else{
    mode="walking";
  }
  for (var i = 0; i < tab.length; i++) {
    if(i>0 && i < tab.length - 1) {
      lat = tab[i][0];
      lng = tab[i][1];
      tab_inter.push([lat,lng]);
      }else {
         var latini = tab[0][0];
         var lngini =tab[0][1];
         var latfin = tab[tab.length-1][0];
         var lngfin =  tab[tab.length-1][1];
         var origine=+latini+','+lngini;
         var destination = latfin+','+lngfin;
      }
    }
    for (var i = 0; i < tab_inter.length; i++) {
      if (i == tab_inter.length-1){
        var lat = tab_inter[i][0];
        var lng = tab_inter[i][1];
        way_points += 'via:'+lat+','+lng;
      }else {
        var lat = tab_inter[i][0];
        var lng = tab_inter[i][1];
        way_points += 'via:'+lat+','+lng+'|';
      }
    }
    var ajax = new XMLHttpRequest();
    ajax.open('GET', 'js/phpgg.php?origine='+origine+"&destination="+destination+"&waypoints="+way_points+"&mode="+mode, true);
    // métadonnées de la requête AJAX
    ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    // evenement de changement d'état de la requête
    ajax.addEventListener('readystatechange',  function(e) {
      // si l'état est le numéro 4 et que la ressource est trouvée
      if(ajax.readyState == 4 && ajax.status == 200) {
        res = JSON.parse(ajax.responseText);
        temps_totale_google = res.routes[0].legs[0].duration.text ;
        distance_totale_google = res.routes[0].legs[0].distance.text ;
        var ligne = res.routes[0].overview_polyline.points;
        decoded_line=decode(ligne);
        map.remove();
        map=null;
        IGN();
        replaceMarker()
        get_OSM();
        get_geoportail()
        initMap();
      }
    });
  ajax.send();
}

// source: https://gist.github.com/ismaels/6636986
function decode(encoded){
    /*
    Cette fonction permet de décoder une polyline encodée par google.
    Param encoded : la polyline encodée
    Return points : la liste des points parmettant de tracer la polyline
    sous le forme d’un tableau de point de la forme [{lat :, lng :},…].
    */
    // array that holds the points
    var points=[ ]
    var index = 0, len = encoded.length;
    var lat = 0, lng = 0;
    while (index < len) {
        var b, shift = 0, result = 0;
        do {

    b = encoded.charAt(index++).charCodeAt(0) - 63;//finds ascii                //and substract it by 63
              result |= (b & 0x1f) << shift;
              shift += 5;
             } while (b >= 0x20);


       var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
       lat += dlat;
      shift = 0;
      result = 0;
     do {
        b = encoded.charAt(index++).charCodeAt(0) - 63;
        result |= (b & 0x1f) << shift;
       shift += 5;
         } while (b >= 0x20);
     var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
     lng += dlng;

   points.push({lat:( lat / 1E5),lng:( lng / 1E5)})

  }
  return points
}
