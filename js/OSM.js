/*
Cette page contient les fonctions de requête et traitement du résultat relatif à l’api OpenRoute.
*/

//LES VARIABLES GLOBALES
var key="58d904a497c67e00015b45fc8aab4aa30d9a44a18d5d0feda3c0e03f";
var mode;
var type;
var infosOSM;
var temps_totale_osm ;
var distance_totale_osm ;
var instruction_osm;

//LES FONCTIONS
function get_OSM(){
  /*
  Cette fonction met en forme les points de cheminement et fait la requête vers
  l’api direction de OSM ; elle affiche aussi les résultats graphiques.
  */
  infosOSM=null;
  temps_totale_osm =null;
  distance_totale_osm=null ;
  console.log(infosOSM);
  document.getElementById("ins").removeAttribute("disabled");
  if (document.getElementById("voiture").checked){
    mode="driving-car";
  }else{
    mode="foot-walking";
  }
  if (document.getElementById("rapide").checked){
    type="fastest";
  }else if (document.getElementById("court").checked){
    type="shortest";
  }
  var coordinate="";
  for (var i=0; i<tab.length;i++){
    if (i==tab.length-1){
      coordinate+=tab[i][1]+","+tab[i][0];
    }else{
      coordinate+=tab[i][1]+","+tab[i][0]+"|";
    }
  }
  var url="https://api.openrouteservice.org/directions?api_key="+key+"&coordinates="+coordinate+"&profile="+mode+"&format=json&language=fr&geometry_format=geojson&units=km&preference="+type;
  var features ;
  var ajax = new XMLHttpRequest() ;
  ajax.open('GET', url);
  ajax.addEventListener('readystatechange',  function(e) {
    if(ajax.readyState == 4 && ajax.status == 200) {
      var res=JSON.parse(ajax.responseText);
      console.log(res) ;
      instruction_osm=res['routes'][0]["segments"];
      infosOSM=res['routes'][0]['summary'];
      temps_totale_osm = precisionRound(infosOSM.duration/60,0) ;
      distance_totale_osm = infosOSM.distance ;
      //document.getElementById('OSM').innerHTML+="Temps : "+precisionRound(infosOSM.duration/60,0)+"min<br>Distance :"+infosOSM.distance+"km <br>";
      var trace=res['routes'][0]['geometry'];
      var t=[];
      for (var i=0; i<trace.coordinates.length; i++){
        t.push(new L.LatLng(trace.coordinates[i][1],trace.coordinates[i][0]));
      }
      mapOSM.remove();
      mapOSM=null;

      var trajet=new L.Polyline(t);
      OSM();
      mapOSM.addLayer(trajet);
      for (var i =0; i<mark.length;i++){
          mark[i].addTo(mapOSM);
      }
    }
  });
  ajax.send();
}
